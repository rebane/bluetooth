#include "gatt_server.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static uint8_t *gatt_server_uuid_ble_base = (uint8_t *)"\x00\x00\x00\x00\x00\x00\x10\x00\x80\x00\x00\x80\x5f\x9b\x34\xfb";
static uint8_t gatt_server_uuid_buffer[16];

static struct gatt_server_handle_struct *gatt_server_alloc_handle(int len);
static void gatt_server_free_handle(struct gatt_server_handle_struct *gh);
static void gatt_server_add_handle(gatt_server_t *gs, struct gatt_server_handle_struct *gh);
static void gatt_server_remove_last_handle(gatt_server_t *gs);
static int gatt_server_uuid_is_ble(void *uuid);
static uint16_t gatt_server_uuid_ble_u16(void *uuid);

void *gatt_server_uuid_ble_r(void *dest, uint16_t uuid){
	memcpy(dest, gatt_server_uuid_ble_base, 16);
	((uint8_t *)dest)[2] = (uuid >> 8) & 0xFF;
	((uint8_t *)dest)[3] = (uuid >> 0) & 0xFF;
	return(dest);
}

void *gatt_server_uuid_ble(uint16_t uuid){
	return(gatt_server_uuid_ble_r(gatt_server_uuid_buffer, uuid));
}

void *gatt_server_uuid_convert_r(void *dest, char *uuid){
	unsigned int d[16], i;
	sscanf(uuid, "%02X%02X%02X%02X-%02X%02X-%02X%02X-%02X%02X-%02X%02X%02X%02X%02X%02X",
		&d[0], &d[1], &d[2], &d[3], &d[4], &d[5], &d[6], &d[7],
		&d[8], &d[9], &d[10], &d[11], &d[12], &d[13], &d[14], &d[15]
	);
	for(i = 0; i < 16; i++){
		((uint8_t *)dest)[i] = d[i];
	}
	return(dest);
}

void *gatt_server_uuid_convert(char *uuid){
	return(gatt_server_uuid_convert_r(gatt_server_uuid_buffer, uuid));
}

gatt_server_t *gatt_server_new(gatt_server_write_func write, gatt_server_event_func event, void *user){
	gatt_server_t *gs;
	gs = malloc(sizeof(gatt_server_t));
	if(gs == NULL)return(NULL);
	gs->write = write;
	gs->event = event;
	gs->user = user;
	gs->mtu = 23;
	gs->handle_added = 0x0000;
	gs->service_handle_added = 0x0000;
	gs->handle_first = NULL;
	gs->handle_last = NULL;
	return(gs);
}

uint16_t gatt_server_add_primary_service(gatt_server_t *gs, void *uuid){
	struct gatt_server_handle_struct *gh;
	gh = gatt_server_alloc_handle(16);
	if(gh == NULL)return(0);
	gh->handle = ++gs->handle_added;

	gatt_server_uuidcpy(gh->value, uuid);

	memcpy(gh->uuid, gatt_server_uuid_ble(0x2800), 16);
	gh->permissions = GATT_SERVER_ATTR_READ;
	gatt_server_add_handle(gs, gh);
	gs->service_handle_added = gh->handle;
	return(gh->handle);
}

uint16_t gatt_server_add_characteristic(gatt_server_t *gs, uint16_t service_handle, void *uuid, uint8_t permissions, const void *value, int len){
	struct gatt_server_handle_struct *gh_char;
	struct gatt_server_handle_struct *gh_value;
	struct gatt_server_handle_struct *gh_decl;

	if(service_handle == 0x0000){
		if(gs->service_handle_added == 0x0000)return(0);
		service_handle = gs->service_handle_added;
	}

	if(gatt_server_uuid_is_ble(uuid)){
		gh_char = gatt_server_alloc_handle(5);
		if(gh_char == NULL)return(0);
		gatt_server_u16_set(gh_char->value, 3, gatt_server_uuid_ble_u16(uuid));
	}else{
		gh_char = gatt_server_alloc_handle(19);
		if(gh_char == NULL)return(0);
		gatt_server_uuidcpy(&gh_char->value[3], uuid);
	}
	gh_value = gatt_server_alloc_handle(len);
	if(gh_value == NULL){
		gatt_server_free_handle(gh_char);
		return(0);
	}
	if(permissions & (GATT_SERVER_PROP_NOTIFY | GATT_SERVER_PROP_INDICATE)){
		gh_decl = gatt_server_alloc_handle(2);
		if(gh_value == NULL){
			gatt_server_free_handle(gh_char);
			gatt_server_free_handle(gh_value);
			return(0);
		}
	}

	gh_char->handle = ++gs->handle_added;
	gh_value->handle = ++gs->handle_added;
	if(permissions & (GATT_SERVER_PROP_NOTIFY | GATT_SERVER_PROP_INDICATE))gh_decl->handle = ++gs->handle_added;

	memcpy(gh_value->uuid, uuid, 16);

	gh_char->value[0] = permissions;
	gatt_server_u16_set(gh_char->value, 1, gh_value->handle);
	memcpy(gh_char->uuid, gatt_server_uuid_ble(0x2803), 16);
	gh_char->permissions = GATT_SERVER_ATTR_READ;
	gh_char->service_handle = service_handle;
	gh_char->decl_handle = 0x0000;
	gatt_server_add_handle(gs, gh_char);

	memcpy(gh_value->value, value, gh_value->len);
	gh_value->permissions = 0;
	if(permissions & GATT_SERVER_PROP_READ)gh_value->permissions |= GATT_SERVER_ATTR_READ;
	if(permissions & (GATT_SERVER_PROP_WRITE_CMD | GATT_SERVER_PROP_WRITE_REQ))gh_value->permissions |= GATT_SERVER_ATTR_WRITE;
	gh_value->service_handle = service_handle;
	gh_value->decl_handle = 0x0000;
	gatt_server_add_handle(gs, gh_value);

	if(permissions & (GATT_SERVER_PROP_NOTIFY | GATT_SERVER_PROP_INDICATE)){
		gatt_server_u16_set(gh_decl->value, 0, 0x0000);
		memcpy(gh_decl->uuid, gatt_server_uuid_ble(0x2902), 16);
		gh_decl->permissions = GATT_SERVER_ATTR_READ | GATT_SERVER_ATTR_WRITE;
		gh_decl->service_handle = service_handle;
		gh_decl->decl_handle = 0x0000;
		gatt_server_add_handle(gs, gh_decl);
		gh_value->decl_handle = gh_decl->handle;
	}

	return(gh_value->handle);
}

void gatt_server_init(gatt_server_t *gs){
	struct gatt_server_handle_struct *gh;
	for(gh = gs->handle_first; gh != NULL; gh = gh->next){
	}
}

void gatt_server_free(gatt_server_t *gs){
	struct gatt_server_handle_struct *gh;
	while(gs->handle_last != NULL){
		gh = gs->handle_last;
		gatt_server_remove_last_handle(gs);
		gatt_server_free_handle(gh);
	}
	free(gs);
}

void gatt_server_u16_set(void *dest, int offset, uint16_t value){
	((uint8_t *)dest)[offset + 0] = (value >> 0) & 0xFF;
	((uint8_t *)dest)[offset + 1] = (value >> 8) & 0xFF;
}

uint16_t gatt_server_u16(void *dest, int offset){
	return(((uint16_t)(((uint8_t *)dest)[offset + 0]) << 0) | ((uint16_t)(((uint8_t *)dest)[offset + 1]) << 8));
}

void *gatt_server_uuidcpy(void *dest, const void *src){
	int i;
	for(i = 0; i < 16; i++){
		((uint8_t *)dest)[i] = ((uint8_t *)src)[15 - i];
	}
	return(dest);
}

int gatt_server_uuidcmp(void *uuid1, void *uuid2){
	return(memcmp(uuid1, uuid2, 16));
}

static struct gatt_server_handle_struct *gatt_server_alloc_handle(int len){
	struct gatt_server_handle_struct *gh;
	gh = malloc(sizeof(struct gatt_server_handle_struct));
	if(gh == NULL)return(NULL);
	gh->value = malloc(len);
	if(gh->value == NULL){
		free(gh);
		return(NULL);
	}
	gh->len = len;
	return(gh);
}

static void gatt_server_free_handle(struct gatt_server_handle_struct *gh){
	free(gh->value);
	free(gh);
}

static void gatt_server_add_handle(gatt_server_t *gs, struct gatt_server_handle_struct *gh){
	if(gs->handle_first == NULL){
		gh->prev = NULL;
		gs->handle_first = gh;
	}else{
		gh->prev = gs->handle_last;
		gh->prev->next = gh;
	}
	gh->next = NULL;
	gs->handle_last = gh;
}

static void gatt_server_remove_last_handle(gatt_server_t *gs){
	if(gs->handle_last == NULL)return;
	if(gs->handle_last->prev == NULL){
		gs->handle_last = NULL;
		gs->handle_first = NULL;
	}else{
		gs->handle_last->prev->next = NULL;
		gs->handle_last = gs->handle_last->prev;
	}
}

static int gatt_server_uuid_is_ble(void *uuid){
	if(memcmp(&((uint8_t *)uuid)[0], &gatt_server_uuid_ble_base[0], 2))return(0);
	if(memcmp(&((uint8_t *)uuid)[4], &gatt_server_uuid_ble_base[4], 12))return(0);
	return(1);
}

static uint16_t gatt_server_uuid_ble_u16(void *uuid){
	return(((uint16_t)(((uint8_t *)uuid)[2]) << 8) | ((uint16_t)(((uint8_t *)uuid)[3]) << 0));
}

static char gatt_server_uuid_cstr_buffer[64];
char *gatt_server_uuid_cstr(void *uuid){
	snprintf(gatt_server_uuid_cstr_buffer, 64, "%02x%02x%02x%02x-%02x%02x-%02x%02x-%02x%02x-%02x%02x%02x%02x%02x%02x",
		(unsigned int)((uint8_t *)uuid)[0],
		(unsigned int)((uint8_t *)uuid)[1],
		(unsigned int)((uint8_t *)uuid)[2],
		(unsigned int)((uint8_t *)uuid)[3],
		(unsigned int)((uint8_t *)uuid)[4],
		(unsigned int)((uint8_t *)uuid)[5],
		(unsigned int)((uint8_t *)uuid)[6],
		(unsigned int)((uint8_t *)uuid)[7],
		(unsigned int)((uint8_t *)uuid)[8],
		(unsigned int)((uint8_t *)uuid)[9],
		(unsigned int)((uint8_t *)uuid)[10],
		(unsigned int)((uint8_t *)uuid)[11],
		(unsigned int)((uint8_t *)uuid)[12],
		(unsigned int)((uint8_t *)uuid)[13],
		(unsigned int)((uint8_t *)uuid)[14],
		(unsigned int)((uint8_t *)uuid)[15]
	);
	gatt_server_uuid_cstr_buffer[63] = 0;
	return(gatt_server_uuid_cstr_buffer);
}

void gatt_server_debug(gatt_server_t *gs){
	struct gatt_server_handle_struct *gh;
	int i, l;
	for(gh = gs->handle_first; gh != NULL; gh = gh->next){
		printf("SERVICE: %04X, HANDLE: %04X, UUID: %s, PERM: %02X, VALUE:",
			(unsigned int)gh->service_handle,
			(unsigned int)gh->handle,
			gatt_server_uuid_cstr(gh->uuid),
			(unsigned int)gh->permissions
		);
		l = gh->len;
		if(l > 32)l = 32;
		for(i = 0; i < l; i++){
			printf(" %02X", (unsigned int)gh->value[i]);
		}
		printf("\n");
	}
}

