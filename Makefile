CROSS = arm-linux-gnueabi-
MTIMER_PARAMS = -I/home/rebane/projects/me/lib2/mtimer /home/rebane/projects/me/lib2/mtimer/mtimer.c /home/rebane/projects/me/lib2/mtimer/timer_linux.c
# BLUETOOTH_PARAMS = -lbluetooth
BLUETOOTH_PARAMS = -lbluetooth -L/home/rebane/projects/fox-iot/fw_nuc980_linux_source/bluez/bluez-5.54/lib

all:
	$(CROSS)gcc -Wall $(MTIMER_PARAMS) mgmt.c hci.c gatt.c gatt_server_database.c gatt_server_protocol.c blinky.c $(BLUETOOTH_PARAMS) -o blinky
	$(CROSS)gcc -Wall mgmt.c mgmt_server.c hci.c sdp.c sdp_server.c rfcomm.c comm.c terminal.c relay.c -DCOMM_TERMINAL $(BLUETOOTH_PARAMS) -lpthread -lutil -o rfcomm_terminal
	$(CROSS)gcc -Wall mgmt.c mgmt_server.c hci.c sdp.c sdp_server.c rfcomm.c comm.c relay.c -DCOMM_RELAY $(BLUETOOTH_PARAMS) -lpthread -lutil -o rfcomm_relay
	$(CROSS)strip blinky
	$(CROSS)strip rfcomm_terminal
	$(CROSS)strip rfcomm_relay

