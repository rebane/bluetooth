#include "relay.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

void relay_init(){
	system("echo low >/dev/gpio/led0/direction");
	system("echo low >/dev/gpio/led1/direction");
	system("echo low >/dev/gpio/led2/direction");
	system("echo low >/dev/gpio/led3/direction");
	system("echo low >/dev/gpio/led4/direction");
	system("echo low >/dev/gpio/led5/direction");
	system("echo low >/dev/gpio/led6/direction");
	system("echo low >/dev/gpio/led7/direction");
}

int relay_loop(int fd){
	unsigned char c;
	int l;

	while(1){
		l = read(fd, &c, 1);
		if(l <= 0)break;
		printf("READ: %02X\n", (unsigned int)c);
		if(c == 0x41){
			system("echo high >/dev/gpio/led0/direction");
		}else if(c == 0x61){
			system("echo low >/dev/gpio/led0/direction");
		}
		if(c == 0x42){
			system("echo high >/dev/gpio/led1/direction");
		}else if(c == 0x62){
			system("echo low >/dev/gpio/led1/direction");
		}
		if(c == 0x43){
			system("echo high >/dev/gpio/led2/direction");
		}else if(c == 0x63){
			system("echo low >/dev/gpio/led2/direction");
		}
		if(c == 0x44){
			system("echo high >/dev/gpio/led3/direction");
		}else if(c == 0x64){
			system("echo low >/dev/gpio/led3/direction");
		}
		if(c == 0x45){
			system("echo high >/dev/gpio/led4/direction");
		}else if(c == 0x65){
			system("echo low >/dev/gpio/led4/direction");
		}
		if(c == 0x46){
			system("echo high >/dev/gpio/led5/direction");
		}else if(c == 0x66){
			system("echo low >/dev/gpio/led5/direction");
		}
		if(c == 0x47){
			system("echo high >/dev/gpio/led6/direction");
		}else if(c == 0x67){
			system("echo low >/dev/gpio/led6/direction");
		}
		if(c == 0x48){
			system("echo high >/dev/gpio/led7/direction");
		}else if(c == 0x68){
			system("echo low >/dev/gpio/led7/direction");
		}
	}
	return(0);
}

