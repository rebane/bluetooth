#ifndef _GATT_SERVER_H_
#define _GATT_SERVER_H_

#include <stdint.h>

#define GATT_SERVER_ATTR_READ          0x01
#define GATT_SERVER_ATTR_WRITE         0x02
#define GATT_SERVER_ATTR_AUTHEN_READ   0x04
#define GATT_SERVER_ATTR_AUTHEN_WRITE  0x08
#define GATT_SERVER_ATTR_AUTHOR_READ   0x10
#define GATT_SERVER_ATTR_AUTHOR_WRITE  0x20
#define GATT_SERVER_ATTR_ENCRYPT_READ  0x40
#define GATT_SERVER_ATTR_ENCRYPT_WRITE 0x80

#define GATT_SERVER_PROP_BROADCAST     0x01
#define GATT_SERVER_PROP_READ          0x02
#define GATT_SERVER_PROP_WRITE_CMD     0x04
#define GATT_SERVER_PROP_WRITE_REQ     0x08
#define GATT_SERVER_PROP_NOTIFY        0x10
#define GATT_SERVER_PROP_INDICATE      0x20
#define GATT_SERVER_PROP_AUTHOR_WRITE  0x40
#define GATT_SERVER_PROP_EXTENDED      0x80

#define GATT_SERVER_CCCD_NOTIFY        0x0001
#define GATT_SERVER_CCCD_INDICATE      0x0002

typedef int (*gatt_server_write_func)(void *user, const void *buf, int count);
typedef int (*gatt_server_event_func)(void *user, int event, uint16_t offset, uint16_t handle, void *data, int count);

struct gatt_server_handle_struct{
	uint16_t handle;
	uint16_t service_handle;
	uint16_t decl_handle;
	uint8_t uuid[16];
	uint8_t permissions;
	uint8_t properties;
	uint8_t *value;
	uint16_t len;
	struct gatt_server_handle_struct *prev;
	struct gatt_server_handle_struct *next;
};

struct gatt_server_struct{
	gatt_server_write_func write;
	gatt_server_event_func event;
	void *user;
	uint16_t mtu;
	uint16_t handle_added;
	uint16_t service_handle_added;
	struct gatt_server_handle_struct *handle_first;
	struct gatt_server_handle_struct *handle_last;
};

typedef struct gatt_server_struct gatt_server_t;

void *gatt_server_uuid_ble_r(void *dest, uint16_t uuid);
void *gatt_server_uuid_ble(uint16_t uuid);
void *gatt_server_uuid_convert_r(void *dest, char *uuid);
void *gatt_server_uuid_convert(char *uuid);

gatt_server_t *gatt_server_new(gatt_server_write_func write, gatt_server_event_func event, void *user);
uint16_t gatt_server_add_primary_service(gatt_server_t *gs, void *uuid);
uint16_t gatt_server_add_characteristic(gatt_server_t *gs, uint16_t service_handle, void *uuid, uint8_t permissions, const void *value, int len);
void gatt_server_init(gatt_server_t *gs);
void gatt_server_free(gatt_server_t *gs);

int gatt_server_feed(gatt_server_t *gs, void *buf, int count);
int gatt_server_value_set(gatt_server_t *gs, uint16_t handle, void *data, int count);

void gatt_server_u16_set(void *dest, int offset, uint16_t value);
uint16_t gatt_server_u16(void *dest, int offset);
void *gatt_server_uuidcpy(void *dest, const void *src);
int gatt_server_uuidcmp(void *uuid1, void *uuid2);

char *gatt_server_uuid_cstr(void *uuid);
void gatt_server_debug(gatt_server_t *gs);

#endif

