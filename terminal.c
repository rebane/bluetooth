#include "terminal.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <poll.h>
#include <pty.h>

void terminal_init(){
}

int terminal_loop(int fd, char *path){
	unsigned char buffer[1024];
	struct pollfd fds[2];
	int fd_term, l;
	pid_t childp;

	childp = forkpty(&fd_term, NULL, NULL, NULL);
	if(childp < 0){
		return(-1);
	}else if(childp == 0){
		execl(path, path, "-l", NULL);
	}
	while(1){
		fds[0].fd = fd;
		fds[0].events = POLLIN;
		fds[1].fd = fd_term;
		fds[1].events = POLLIN;
		l = poll(fds, 2, 1000);
		if(l < 0)break;
		if(l == 0)continue;
		if(fds[0].revents & POLLIN){
			l = read(fd, buffer, 1024);
			if(l <= 0)break;
			write(fd_term, buffer, l);
		}
		if(fds[1].revents & POLLIN){
			l = read(fd_term, buffer, 1024);
			if(l <= 0)break;
			write(fd, buffer, l);
		}
		if((fds[0].revents & (POLLHUP | POLLERR | POLLNVAL)) || (fds[1].revents & (POLLHUP | POLLERR | POLLNVAL)))break;
	}
	close(fd_term);
	return(0);
}

