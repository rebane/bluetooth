#include "gatt.h"
#include <unistd.h>
#include <string.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <bluetooth/l2cap.h>

#ifndef ATT_CID
#define ATT_CID 4
#endif

int gatt_open(int dev_id, int addr_type, int sec){
	int fd;
	struct sockaddr_l2 addr_l2;

	fd = socket(PF_BLUETOOTH, SOCK_SEQPACKET, BTPROTO_L2CAP);
	if(fd < 0)return(fd);

	memset(&addr_l2, 0, sizeof(addr_l2));
	addr_l2.l2_family = AF_BLUETOOTH;
	addr_l2.l2_bdaddr_type = BDADDR_LE_PUBLIC;
	hci_devba(dev_id, &addr_l2.l2_bdaddr);
	addr_l2.l2_cid = htobs(ATT_CID);

	if(bind(fd, (struct sockaddr *)&addr_l2, sizeof(addr_l2)) < 0){
		close(fd);
		return(-1);
	}
	return(fd);
}

