#include "hci.h"
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <errno.h>

int hci_le_send_req(int fd, unsigned int command, void *buf, int count, int to){
	struct hci_request rq;
	uint8_t status;
	
	memset(&rq, 0, sizeof(rq));
	rq.ogf = OGF_LE_CTL;
	rq.ocf = command;
	rq.cparam = buf;
	rq.clen = count;
	rq.rparam = &status;
	rq.rlen = 1;
	
	if(hci_send_req(fd, &rq, to) < 0)return(-1);
	
	if(status){
//		printf("S: %u\n", (unsigned int)status);
	        errno = EIO;
	        return(-1);
	}
	
	return(0);
}

int hci_le_set_advertising_data(int fd, void *buf, int count, int to){
	unsigned char buffer[32];

	if(count < 0)return(count);
	if(count > 31)return(-1);

	memset(buffer, 0, 32);
	buffer[0] = count;
	memmove(&buffer[1], buf, count);

	return(hci_le_send_req(fd, OCF_LE_SET_ADVERTISING_DATA, buffer, 32, to));
}

int hci_le_set_scan_response_data(int fd, void *buf, int count, int to){
	unsigned char buffer[32];

	if(count < 0)return(count);
	if(count > 31)return(-1);

	memset(buffer, 0, 32);
	buffer[0] = count;
	memmove(&buffer[1], buf, count);

	return(hci_le_send_req(fd, OCF_LE_SET_SCAN_RESPONSE_DATA, buffer, 32, to));
}

int hci_scan_set(int fd, int dev_id, int opt){
	struct hci_dev_req dr;
	dr.dev_id = dev_id;
	dr.dev_opt = opt;
	return(ioctl(fd, HCISETSCAN, (unsigned long)&dr));
}

