#include "sdp.h"
#include <unistd.h>
#include <string.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <bluetooth/l2cap.h>
#include <bluetooth/sdp.h>

int sdp_open(int dev_id){
	int fd;
	struct sockaddr_l2 addr_l2;

	fd = socket(PF_BLUETOOTH, SOCK_SEQPACKET, BTPROTO_L2CAP);
	if(fd < 0)return(fd);

	memset(&addr_l2, 0, sizeof(addr_l2));
	addr_l2.l2_family = AF_BLUETOOTH;
	hci_devba(dev_id, &addr_l2.l2_bdaddr);
	addr_l2.l2_psm = htobs(SDP_PSM);

	if(bind(fd, (struct sockaddr *)&addr_l2, sizeof(addr_l2)) < 0){
		close(fd);
		return(-1);
	}
	return(fd);
}

/*02 00 01 E4 00 E0 00 49 00 07 00 00 00 DB 00 D8 35 D6 35 D4 
  09 00 00 0A 00 01 00 03 09 00 01 35 03 19 11 01 09 00 02 0A 
  00 00 12 34 09 00 03 19 11 01 09 00 04 35 0C 35 03 19 01 00 
  35 05 19 00 03 08 01 09 00 05 35 03 19 10 02 09 00 06 35 09 
  09 65 6E 09 00 6A 09 01 00 09 00 07 0A 00 00 FF FF 09 00 08 
  08 FF 09 00 09 35 08 35 06 19 11 01 09 01 00 09 00 0A 45 15 
  68 74 74 70 3A 2F 2F 77 77 77 2E 62 6C 75 65 7A 2E 6F 72 67 
  2F 09 00 0B 45 15 68 74 74 70 3A 2F 2F 77 77 77 2E 62 6C 75 
  65 7A 2E 6F 72 67 2F 09 00 0C 45 15 68 74 74 70 3A 2F 2F 77 
  77 77 2E 62 6C 75 65 7A 2E 6F 72 67 2F 09 01 00 25 0B 53 65 
  72 69 61 6C 20 50 6F 72 74 09 01 01 25 08 43 4F 4D 20 50 6F 
  72 74 09 01 02 25 05 42 6C 75 65 5A 00 */
