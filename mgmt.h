#ifndef _MGMT_H_
#define _MGMT_H_

int mgmt_open();
int mgmt_cmd(int fd, int dev_id, unsigned int command, void *buf, int count);
int mgmt_cmd_set(int fd, int dev_id, unsigned int command, int on);
int mgmt_discoverable_set(int fd, int dev_id, int on, int timeout);
int mgmt_fast_connectable_set(int fd, int dev_id, int on);
int mgmt_pairable_set(int fd, int dev_id, int on);
int mgmt_link_security_set(int fd, int dev_id, int on);
int mgmt_secure_simple_pairing_set(int fd, int dev_id, int on);
int mgmt_high_set(int fd, int dev_id, int on);
int mgmt_le_set(int fd, int dev_id, int on);
int mgmt_local_name(int fd, int dev_id, char *name, char *short_name);
int mgmt_connectable_set(int fd, int dev_id, int on);
int mgmt_bredr_set(int fd, int dev_id, int on);
int mgmt_advertising_set(int fd, int dev_id, int on);

#endif

