#include "sdp_server.h"
#include "sdp.h"
#include <unistd.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <pthread.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <bluetooth/l2cap.h>
#include <errno.h>

static void *sdp_server_accept_thread(void *p);
static void *sdp_server_thread(void *q);

int sdp_server_start(int fd){
	pthread_t thread;
	void *p;
	p = malloc(sizeof(int));
	if(p == NULL)return(-1);
	*(int *)p = fd;
	if(pthread_create(&thread, NULL, sdp_server_accept_thread, p) != 0){
		free(p);
		return(-1);
	}
	return(0);
}

static void *sdp_server_accept_thread(void *p){
	struct sockaddr_l2 addr_l2;
	int fd, fd_client;
	pthread_t thread;
	socklen_t s;
	void *q;

	fd = *(int *)p;

	while(1){
		s = sizeof(addr_l2);
		fd_client = accept(fd, (struct sockaddr *)&addr_l2, &s);
		if(fd_client < 0)break;
		printf("SDP ACCEPTED\n");
		q = malloc(sizeof(int));
		if(q == NULL)continue;
		*(int *)q = fd_client;
		if(pthread_create(&thread, NULL, sdp_server_thread, q) != 0)free(q);
	}
	printf("SDP STOPPED: %s\n", strerror(errno));
	shutdown(fd, SHUT_RDWR);
	close(fd);
	free(p);
	return(NULL);
}

static void *sdp_server_thread(void *q){
	unsigned char buffer[1024];
	int fd, l, i;

	fd = *(int *)q;

	while(1){
		l = read(fd, buffer, 1024);
		if(l <= 0)break;
		// READ: 06 00 00 00 0F 35 03 19 11 01 03 F0 35 05 0A 00 00 FF FF 00
		printf("SDP READ:");
		for(i = 0; i < l; i++){
			printf(" %02X", (unsigned int)buffer[i]);
		}
		printf("\n");
		if(buffer[0] == 0x06){
			write(fd, "\x07\x00\x00\x00\xDB\x00\xD8\x35\xD6\x35\xD4\x09\x00\x00\x0A\x00\x01\x00\x03\x09\x00\x01\x35\x03\x19\x11\x01\x09\x00\x02\x0A\x00\x00\x12\x34\x09\x00\x03\x19\x11\x01\x09\x00\x04\x35\x0C\x35\x03\x19\x01\x00\x35\x05\x19\x00\x03\x08\x01\x09\x00\x05\x35\x03\x19\x10\x02\x09\x00\x06\x35\x09\x09\x65\x6E\x09\x00\x6A\x09\x01\x00\x09\x00\x07\x0A\x00\x00\xFF\xFF\x09\x00\x08\x08\xFF\x09\x00\x09\x35\x08\x35\x06\x19\x11\x01\x09\x01\x00\x09\x00\x0A\x45\x15\x68\x74\x74\x70\x3A\x2F\x2F\x77\x77\x77\x2E\x62\x6C\x75\x65\x7A\x2E\x6F\x72\x67\x2F\x09\x00\x0B\x45\x15\x68\x74\x74\x70\x3A\x2F\x2F\x77\x77\x77\x2E\x62\x6C\x75\x65\x7A\x2E\x6F\x72\x67\x2F\x09\x00\x0C\x45\x15\x68\x74\x74\x70\x3A\x2F\x2F\x77\x77\x77\x2E\x62\x6C\x75\x65\x7A\x2E\x6F\x72\x67\x2F\x09\x01\x00\x25\x0B\x53\x65\x72\x69\x61\x6C\x20\x50\x6F\x72\x74\x09\x01\x01\x25\x08\x43\x4F\x4D\x20\x50\x6F\x72\x74\x09\x01\x02\x25\x05\x42\x6C\x75\x65\x5A\x00", 225);
		}
	}
	printf("SDP CLOSED\n");
	shutdown(fd, SHUT_RDWR);
	close(fd);
	free(q);
	return(NULL);
}

