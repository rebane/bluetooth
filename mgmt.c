#include "mgmt.h"
#include <unistd.h>
#include <string.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <errno.h>

int mgmt_open(){
	int fd;
	struct sockaddr_hci addr_hci;

	fd = socket(AF_BLUETOOTH, SOCK_RAW | SOCK_CLOEXEC | SOCK_NONBLOCK, BTPROTO_HCI);
	if(fd < 0)return(fd);

	memset(&addr_hci, 0, sizeof(addr_hci));
	addr_hci.hci_family = AF_BLUETOOTH;
	addr_hci.hci_dev = HCI_DEV_NONE;
	addr_hci.hci_channel = HCI_CHANNEL_CONTROL;

	if(bind(fd, (struct sockaddr *)&addr_hci, sizeof(addr_hci)) < 0){
		close(fd);
		return(-1);
	}
	return(fd);
}

int mgmt_cmd(int fd, int dev_id, unsigned int command, void *buf, int count){
	unsigned char buffer[6 + 300];
	unsigned int l, u;
	int i;

	if(count < 0)return(count);
	if(count > 300)return(-1);
	while(1){
		i = read(fd, buffer, 6 + 300);
		if(i < 0)break;
		printf("MGMT FLUSH\n");
	}
	buffer[0] = (command >> 0) & 0xFF;
	buffer[1] = (command >> 8) & 0xFF;
	buffer[2] = (dev_id >> 0) & 0xFF;
	buffer[3] = (dev_id >> 8) & 0xFF;
	buffer[4] = (count >> 0) & 0xFF;
	buffer[5] = (count >> 8) & 0xFF;
	memcpy(&buffer[6], buf, count);
	i = write(fd, buffer, 6 + count);
	if(i < 0)return(i);
	if(i != (6 + count))return(-1);

	while(1){
		i = read(fd, buffer, 6 + 300);
		if(i < 0)return(i);
		if(i < 6)continue;
		u = ((unsigned int)buffer[0] << 0) | ((unsigned int)buffer[1] << 8);
		if(u != 0x0001)continue;
		u = ((unsigned int)buffer[2] << 0) | ((unsigned int)buffer[3] << 8);
		if(u != dev_id)continue;
		l = ((unsigned int)buffer[4] << 0) | ((unsigned int)buffer[5] << 8);
		if(l < 3)continue;
		if(i < (6 + l))continue;
		u = ((unsigned int)buffer[6] << 0) | ((unsigned int)buffer[7] << 8);
		if(u != command)continue;
		if(buffer[8] != 0x00)return(-1);
		return(0);
	}
	return(0);
}

int mgmt_cmd_set(int fd, int dev_id, unsigned int command, int on){
	unsigned char c = 0;
	if(on)c = 1;
	return(mgmt_cmd(fd, dev_id, command, &c, 1));
}

int mgmt_discoverable_set(int fd, int dev_id, int on, int timeout){
	unsigned char buffer[3];
	buffer[0] = 0;
	if(on)buffer[0] = 1;
	buffer[1] = (timeout >> 0) & 0xFF;
	buffer[2] = (timeout >> 8) & 0xFF;
	return(mgmt_cmd(fd, dev_id, 0x0006, buffer, 3));
}

int mgmt_fast_connectable_set(int fd, int dev_id, int on){
	return(mgmt_cmd_set(fd, dev_id, 0x0008, on));
}

int mgmt_pairable_set(int fd, int dev_id, int on){
	return(mgmt_cmd_set(fd, dev_id, 0x0009, on));
}

int mgmt_link_security_set(int fd, int dev_id, int on){
	return(mgmt_cmd_set(fd, dev_id, 0x000A, on));
}

int mgmt_secure_simple_pairing_set(int fd, int dev_id, int on){
	return(mgmt_cmd_set(fd, dev_id, 0x000B, on));
}

int mgmt_high_set(int fd, int dev_id, int on){
	return(mgmt_cmd_set(fd, dev_id, 0x000C, on));
}

int mgmt_le_set(int fd, int dev_id, int on){
	return(mgmt_cmd_set(fd, dev_id, 0x000D, on));
}

int mgmt_local_name(int fd, int dev_id, char *name, char *short_name){
	char buffer[249 + 11];
	memset(buffer, 0, 249 + 11);
	strncpy(&buffer[0], name, 248);
	strncpy(&buffer[249], short_name, 10);
	return(mgmt_cmd(fd, dev_id, 0x000F, buffer, 249 + 11));
}

int mgmt_connectable_set(int fd, int dev_id, int on){
	return(mgmt_cmd_set(fd, dev_id, 0x0007, on));
}

int mgmt_bredr_set(int fd, int dev_id, int on){
	return(mgmt_cmd_set(fd, dev_id, 0x002A, on));
}

int mgmt_advertising_set(int fd, int dev_id, int on){
	return(mgmt_cmd_set(fd, dev_id, 0x0029, on));
}

