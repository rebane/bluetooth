#include "gatt_server.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// #define printf(...)

static int gatt_server_exchange_mtu_request(gatt_server_t *gs, uint16_t mtu);
static int gatt_server_find_information_request(gatt_server_t *gs, uint16_t start_handle, uint16_t end_handle);
static int gatt_server_find_by_type_value_request(gatt_server_t *gs, uint16_t start_handle, uint16_t end_handle, void *uuid, void *data, int count);
static int gatt_server_read_by_type_request(gatt_server_t *gs, uint16_t start_handle, uint16_t end_handle, void *uuid);
static int gatt_server_read_request(gatt_server_t *gs, uint16_t handle);
static int gatt_server_read_blob_request(gatt_server_t *gs, uint16_t handle, uint16_t offset);
static int gatt_server_read_by_group_type_request(gatt_server_t *gs, uint16_t start_handle, uint16_t end_handle, void *uuid);
static int gatt_server_write_request(gatt_server_t *gs, uint16_t handle, void *data, int count);
static int gatt_server_write_command(gatt_server_t *gs, uint16_t handle, void *data, int count);
static int gatt_server_error_response(gatt_server_t *gs, uint8_t opcode, uint16_t handle, uint8_t error);
static uint16_t gatt_server_end_group_handle(gatt_server_t *gs, uint16_t service_handle);
static struct gatt_server_handle_struct *gatt_server_find_handle(gatt_server_t *gs, uint16_t handle);

int gatt_server_feed(gatt_server_t *gs, void *buf, int count){
	uint8_t opcode;
	uint8_t uuid[16];
	if(!count)return(0);
	opcode = ((uint8_t *)buf)[0];
	if(opcode == 0x02){
		if(count == 3)return(gatt_server_exchange_mtu_request(gs, gatt_server_u16(buf, 1)));
	}else if(opcode == 0x04){
		if(count == 5)return(gatt_server_find_information_request(gs, gatt_server_u16(buf, 1), gatt_server_u16(buf, 3)));
	}else if(opcode == 0x06){
		// READ: 23, 06 01 00 FF FF 00 28 31 32 33 34 35 36 37 38 31 32 33 34 35 36 37 38
		if(count >= 7)return(gatt_server_find_by_type_value_request(gs, gatt_server_u16(buf, 1), gatt_server_u16(buf, 3), gatt_server_uuid_ble_r(uuid, gatt_server_u16(buf, 5)), &((uint8_t *)buf)[7], count - 7));
	}else if(opcode == 0x08){
		if(count == 7)return(gatt_server_read_by_type_request(gs, gatt_server_u16(buf, 1), gatt_server_u16(buf, 3), gatt_server_uuid_ble_r(uuid, gatt_server_u16(buf, 5))));
		if(count == 21)return(gatt_server_read_by_type_request(gs, gatt_server_u16(buf, 1), gatt_server_u16(buf, 3), gatt_server_uuidcpy(uuid, &((uint8_t *)buf)[5])));
	}else if(opcode == 0x0A){
		if(count == 3)return(gatt_server_read_request(gs, gatt_server_u16(buf, 1)));
	}else if(opcode == 0x0C){
		if(count == 5)return(gatt_server_read_blob_request(gs, gatt_server_u16(buf, 1), gatt_server_u16(buf, 3)));
	}else if(opcode == 0x10){
		if(count == 7)return(gatt_server_read_by_group_type_request(gs, gatt_server_u16(buf, 1), gatt_server_u16(buf, 3), gatt_server_uuid_ble_r(uuid, gatt_server_u16(buf, 5))));
		if(count == 21)return(gatt_server_read_by_group_type_request(gs, gatt_server_u16(buf, 1), gatt_server_u16(buf, 3), gatt_server_uuidcpy(uuid, &((uint8_t *)buf)[5])));
	}else if(opcode == 0x12){
		if(count >= 3)return(gatt_server_write_request(gs, gatt_server_u16(buf, 1), &((uint8_t *)buf)[3], count - 3));
	}else if(opcode == 0x52){
		if(count >= 3)return(gatt_server_write_command(gs, gatt_server_u16(buf, 1), &((uint8_t *)buf)[3], count - 3));
	}
	return(0);
}

int gatt_server_value_set(gatt_server_t *gs, uint16_t handle, void *data, int count){
	struct gatt_server_handle_struct *gh_value;
	struct gatt_server_handle_struct *gh_decl;
	uint16_t cccd_value;
	uint8_t buffer[512];
	int l;

	gh_value = gatt_server_find_handle(gs, handle);
	if(gh_value == NULL)return(-1);
	if(count > gh_value->len)count = gh_value->len;
	memcpy(gh_value->value, data, count);

	if(gh_value->decl_handle == 0x0000)return(count);
	gh_decl = gatt_server_find_handle(gs, gh_value->decl_handle);
	if(gh_decl == NULL)return(count);
	cccd_value = gatt_server_u16(gh_decl->value, 0);
	if(cccd_value & 0x0001){
		buffer[0] = 0x1B;
		l = gh_value->len;
		if(l > (gs->mtu - 3))l = gs->mtu - 3;
		gatt_server_u16_set(buffer, 1, gh_value->handle);
		memcpy(&buffer[3], gh_value->value, l);
		if(gs->write(gs->user, buffer, 3 + l) < 0)return(-1);
	}
	if(cccd_value & 0x0002){
		buffer[0] = 0x1D;
		l = gh_value->len;
		if(l > (gs->mtu - 3))l = gs->mtu - 3;
		gatt_server_u16_set(buffer, 1, gh_value->handle);
		memcpy(&buffer[3], gh_value->value, l);
		if(gs->write(gs->user, buffer, 3 + l) < 0)return(-1);
	}
	return(count);
}

static int gatt_server_exchange_mtu_request(gatt_server_t *gs, uint16_t mtu){
	printf("exchange mtu request, mtu: %u\n", (unsigned int)mtu);
	return(0);
}

static int gatt_server_find_information_request(gatt_server_t *gs, uint16_t start_handle, uint16_t end_handle){
	struct gatt_server_handle_struct *gh;
	uint8_t buffer[512];
	int l;
	printf("find information request, start: 0x%04x, end: 0x%04x\n", (unsigned int)start_handle, (unsigned int)end_handle);

	l = 0;
	for(gh = gs->handle_first; gh != NULL; gh = gh->next){
		if(gh->handle < start_handle)continue;
		if(gh->handle > end_handle)continue;
		if(l == 0){
			l = 2;
		}
		gatt_server_u16_set(buffer, l, gh->handle);
		gatt_server_uuidcpy(&buffer[l + 2], gh->uuid);
		l += 18;
		if((gs->mtu - l) < 18)break;
	}
	if(l == 0)return(gatt_server_error_response(gs, 0x04, start_handle, 0x0a));
	buffer[0] = 0x05;
	buffer[1] = 0x02;
	return(gs->write(gs->user, buffer, l));
}

static int gatt_server_find_by_type_value_request(gatt_server_t *gs, uint16_t start_handle, uint16_t end_handle, void *uuid, void *data, int count){
	int l;
	printf("find by type value request, start: 0x%04x, end: 0x%04x, uuid: %s, value:", (unsigned int)start_handle, (unsigned int)end_handle, gatt_server_uuid_cstr(uuid));
	for(l = 0; l < count; l++){
		printf(" %02x", (unsigned int)((uint8_t *)data)[l]);
	}
	printf("\n");
	return(0);
}

static int gatt_server_read_by_type_request(gatt_server_t *gs, uint16_t start_handle, uint16_t end_handle, void *uuid){
	struct gatt_server_handle_struct *gh;
	uint8_t buffer[512];
	int l;

	printf("read by type request, start: 0x%04x, end: 0x%04x, uuid: %s\n", (unsigned int)start_handle, (unsigned int)end_handle, gatt_server_uuid_cstr(uuid));
	l = 0;
	for(gh = gs->handle_first; gh != NULL; gh = gh->next){
		if(gh->handle < start_handle)continue;
		if(gh->handle > end_handle)continue;
		if(gatt_server_uuidcmp(gh->uuid, uuid))continue;
		if(l == 0){
			l = 2;
			buffer[1] = 2 + gh->len;
			if(buffer[1] > (gs->mtu - 2))buffer[1] = gs->mtu - 2;
		}else if(gh->len != (buffer[1] - 2)){
			break;
		}
		gatt_server_u16_set(buffer, l, gh->handle);
		memcpy(&buffer[l + 2], gh->value, (buffer[1] - 2));
		l += buffer[1];
		if((gs->mtu - l) < buffer[1])break;
	}
	if(l == 0)return(gatt_server_error_response(gs, 0x08, start_handle, 0x0a));
	buffer[0] = 0x09;
	return(gs->write(gs->user, buffer, l));
}

static int gatt_server_read_request(gatt_server_t *gs, uint16_t handle){
	struct gatt_server_handle_struct *gh;
	uint8_t buffer[512];
	int l;
	printf("read request, handle: 0x%04x\n", (unsigned int)handle);

	gh = gatt_server_find_handle(gs, handle);
	if(gh == NULL)return(gatt_server_error_response(gs, 0x0A, handle, 0x0a));
	buffer[0] = 0x0B;
	l = gh->len;
	if(l > (gs->mtu - 1))l = gs->mtu - 1;
	memcpy(&buffer[1], gh->value, l);
	return(gs->write(gs->user, buffer, 1 + l));
}

static int gatt_server_read_blob_request(gatt_server_t *gs, uint16_t handle, uint16_t offset){
	printf("read blob request, handle: 0x%04x, offset: %u\n", (unsigned int)handle, (unsigned int)offset);
	return(0);
}

static int gatt_server_read_by_group_type_request(gatt_server_t *gs, uint16_t start_handle, uint16_t end_handle, void *uuid){
	struct gatt_server_handle_struct *gh;
	uint8_t buffer[512];
	int l;
	printf("read by group type request, start: 0x%04x, end: 0x%04x, uuid: %s\n", (unsigned int)start_handle, (unsigned int)end_handle, gatt_server_uuid_cstr(uuid));

	l = 0;
	for(gh = gs->handle_first; gh != NULL; gh = gh->next){
		if(gh->handle < start_handle)continue;
		if(gh->handle > end_handle)continue;
		if(gatt_server_uuidcmp(gh->uuid, uuid))continue;
		if(l == 0){
			l = 2;
			buffer[1] = 4 + gh->len;
			if(buffer[1] > (gs->mtu - 2))buffer[1] = gs->mtu - 2;
		}else if(gh->len != (buffer[1] - 4)){
			break;
		}
		gatt_server_u16_set(buffer, l, gh->handle);
		gatt_server_u16_set(buffer, l + 2, gatt_server_end_group_handle(gs, gh->handle));
		memcpy(&buffer[l + 4], gh->value, (buffer[1] - 4));
		l += buffer[1];
		if((gs->mtu - l) < buffer[1])break;
	}
	if(l == 0)return(gatt_server_error_response(gs, 0x10, start_handle, 0x0a));
	buffer[0] = 0x11;
	return(gs->write(gs->user, buffer, l));
}

static int gatt_server_write_request(gatt_server_t *gs, uint16_t handle, void *data, int count){
	struct gatt_server_handle_struct *gh;
	uint8_t buffer[1];
	int l;
	printf("write request, handle: 0x%04x, data:", (unsigned int)handle);
	for(l = 0; l < count; l++){
		printf(" %02x", (unsigned int)((uint8_t *)data)[l]);
	}
	printf("\n");

	gh = gatt_server_find_handle(gs, handle);
	if(gh == NULL)return(gatt_server_error_response(gs, 0x12, handle, 0x0a));
	if(!(gh->permissions & GATT_SERVER_ATTR_WRITE))return(gatt_server_error_response(gs, 0x12, handle, 0x0a));
	gs->event(gs->user, 0, gh->handle, 0, data, count);
	l = count;
	if(l > gh->len)l = gh->len;
	memcpy(gh->value, data, l);

	buffer[0] = 0x13;
	return(gs->write(gs->user, buffer, 1));
}

static int gatt_server_write_command(gatt_server_t *gs, uint16_t handle, void *data, int count){
	struct gatt_server_handle_struct *gh;
	int l;
	printf("write command, handle: 0x%04x, data:", (unsigned int)handle);
	for(l = 0; l < count; l++){
		printf(" %02x", (unsigned int)((uint8_t *)data)[l]);
	}
	printf("\n");

	gh = gatt_server_find_handle(gs, handle);
	if(gh == NULL)return(gatt_server_error_response(gs, 0x52, handle, 0x0a));
	if(!(gh->permissions & GATT_SERVER_ATTR_WRITE))return(gatt_server_error_response(gs, 0x52, handle, 0x0a));
	gs->event(gs->user, 0, gh->handle, 0, data, count);
	l = count;
	if(l > gh->len)l = gh->len;
	memcpy(gh->value, data, l);

	return(0);
}

static int gatt_server_error_response(gatt_server_t *gs, uint8_t opcode, uint16_t handle, uint8_t error){
	uint8_t buffer[5];
	buffer[0] = 0x01;
	buffer[1] = opcode;
	gatt_server_u16_set(buffer, 2, handle);
	buffer[4] = error;
	return(gs->write(gs->user, buffer, 5));
}

static uint16_t gatt_server_end_group_handle(gatt_server_t *gs, uint16_t service_handle){
	struct gatt_server_handle_struct *gh;
	uint16_t end_group_handle = 0x0000;
	for(gh = gs->handle_first; gh != NULL; gh = gh->next){
		if(gh->service_handle == service_handle){
			if(end_group_handle < gh->handle){
				end_group_handle = gh->handle;
			}
		}
	}
	return(end_group_handle);
}

static struct gatt_server_handle_struct *gatt_server_find_handle(gatt_server_t *gs, uint16_t handle){
	struct gatt_server_handle_struct *gh;
	for(gh = gs->handle_first; gh != NULL; gh = gh->next){
		if(gh->handle == handle)return(gh);
	}
	return(NULL);
}

