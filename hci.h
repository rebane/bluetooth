#ifndef _HCI_H_
#define _HCI_H_

int hci_le_send_req(int fd, unsigned int command, void *buf, int count, int to);
int hci_le_set_advertising_data(int fd, void *buf, int count, int to);
int hci_le_set_scan_response_data(int fd, void *buf, int count, int to);
int hci_scan_set(int fd, int dev_id, int opt);

#endif

