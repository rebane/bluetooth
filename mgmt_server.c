#include "mgmt_server.h"
#include "mgmt.h"
#include <unistd.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <pthread.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <bluetooth/l2cap.h>
#include <poll.h>

static void *mgmt_server_thread(void *p);

int mgmt_server_start(int fd){
	pthread_t thread;
	void *p;
	p = malloc(sizeof(int));
	if(p == NULL)return(-1);
	*(int *)p = fd;
	if(pthread_create(&thread, NULL, mgmt_server_thread, p) != 0){
		free(p);
		return(-1);
	}
	return(0);
}

static void *mgmt_server_thread(void *p){
	unsigned char buffer[1024];
	struct pollfd fds[1];
	uint16_t dev_id;
	int fd, i, l;

	fd = *(int *)p;

	while(1){
		fds[0].fd = fd;
		fds[0].events = POLLIN;
		l = poll(fds, 1, 1000);
		if(l < 0)break;
		if(l == 0)continue;
		if(fds[0].revents & POLLIN){
			l = read(fd, buffer, 1024);
			if(l <= 0)break;
			printf("MGMT READ:");
			for(i = 0; i < l; i++){
				printf(" %02X", (unsigned int)buffer[i]);
			}
			printf("\n");
			if(l >= 6){
				if((buffer[0] == 0x0F) && (buffer[1] == 0x00) && (l >= 18)){
					printf("need confirmation: 0x%02X\n", (unsigned int)buffer[13]);
					dev_id = ((uint16_t)buffer[2] << 0) | ((uint16_t)buffer[3] << 8);
					mgmt_cmd(fd, dev_id, 0x001C, &buffer[6], 7);
				}
			}
		}
		if((fds[0].revents & (POLLHUP | POLLERR | POLLNVAL)))break;
	}
	free(p);
	return(NULL);
}

