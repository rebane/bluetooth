#include "rfcomm.h"
#include <unistd.h>
#include <string.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <bluetooth/rfcomm.h>

int rfcomm_open(int dev_id, int channel){
	int fd;
	struct sockaddr_rc addr_rc;

	fd = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);
	if(fd < 0)return(fd);

	memset(&addr_rc, 0, sizeof(addr_rc));
	addr_rc.rc_family = AF_BLUETOOTH;
	hci_devba(dev_id, &addr_rc.rc_bdaddr);
	addr_rc.rc_channel = (uint8_t)channel;

	if(bind(fd, (struct sockaddr *)&addr_rc, sizeof(addr_rc)) < 0){
		close(fd);
		return(-1);
	}
	return(fd);
}

