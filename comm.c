#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <bluetooth/rfcomm.h>
#include <signal.h>
#include <errno.h>
#include "mgmt.h"
#include "mgmt_server.h"
#include "hci.h"
#include "sdp.h"
#include "sdp_server.h"
#include "rfcomm.h"
#include "terminal.h"
#include "relay.h"

// https://www.tarlogic.com/en/blog/interactive-shell-via-bluetooth/

int main(int argc, char *argv[]){
	int fd_mgmt, fd_hci, fd_sdp, fd_rfcomm, fd_client;
	struct sockaddr_rc addr_rc;
	socklen_t s;
	int dev_id;

	signal(SIGCHLD, SIG_IGN);

#ifdef COMM_TERMINAL
		terminal_init();
#endif
#ifdef COMM_RELAY
		relay_init();
#endif

	dev_id = hci_devid(argv[1]);
	if(dev_id < 0){
		if(!strncmp(argv[1], "hci", 3)){
			dev_id = atoi(argv[1] + 3);
		}
	}

	if(dev_id < 0){
		fprintf(stderr, "dev id\n");
		return(1);
	}

	fd_mgmt = mgmt_open();
	if(fd_mgmt < 0){
		fprintf(stderr, "mgmt open\n");
		return(1);
	}

	fd_hci = hci_open_dev(dev_id);
	if(fd_hci < 0){
		fprintf(stderr, "hci open\n");
		return(1);
	}

	if(ioctl(fd_hci, HCIDEVDOWN, NULL) < 0){
		fprintf(stderr, "ifdown\n");
		return(1);
	}

	mgmt_high_set(fd_mgmt, dev_id, 0);

#ifdef COMM_TERMINAL
	if(mgmt_local_name(fd_mgmt, dev_id, "TERMINAL", "TERMINAL") < 0){
		fprintf(stderr, "local name\n");
		return(1);
	}
#endif
#ifdef COMM_RELAY
	if(mgmt_local_name(fd_mgmt, dev_id, "RELAY", "RELAY") < 0){
		fprintf(stderr, "local name\n");
		return(1);
	}
#endif

	mgmt_bredr_set(fd_mgmt, dev_id, 1);

	if(mgmt_le_set(fd_mgmt, dev_id, 0) < 0){
		fprintf(stderr, "le set off\n");
		return(1);
	}

	if(mgmt_connectable_set(fd_mgmt, dev_id, 1) < 0){
		fprintf(stderr, "connectable set on\n");
		return(1);
	}

	if(mgmt_discoverable_set(fd_mgmt, dev_id, 1, 0) < 0){
		fprintf(stderr, "discoverable set on\n");
		return(1);
	}

	if(mgmt_fast_connectable_set(fd_mgmt, dev_id, 1) < 0){
		fprintf(stderr, "fast connectable set on\n");
		return(1);
	}

	if(mgmt_pairable_set(fd_mgmt, dev_id, 1) < 0){
		fprintf(stderr, "pairable set on\n");
		return(1);
	}

	if(mgmt_link_security_set(fd_mgmt, dev_id, 0) < 0){
		fprintf(stderr, "link security set off\n");
		return(1);
	}

	if(mgmt_secure_simple_pairing_set(fd_mgmt, dev_id, 1) < 0){
		fprintf(stderr, "simple pairing set on\n");
		return(1);
	}

	if(ioctl(fd_hci, HCIDEVUP, NULL) < 0){
		fprintf(stderr, "ifup\n");
		return(1);
	}

	fd_sdp = sdp_open(dev_id);
	if(fd_sdp < 0){
		fprintf(stderr, "sdp open\n");
		return(1);
	}

	if(listen(fd_sdp, 5) < 0){
		fprintf(stderr, "sdp listen\n");
		return(1);
	}

	if(sdp_server_start(fd_sdp) < 0){
		fprintf(stderr, "sdp start\n");
		return(1);
	}

	if(hci_scan_set(fd_hci, dev_id, SCAN_PAGE | SCAN_INQUIRY) < 0){
		fprintf(stderr, "scan set on\n");
		return(1);
	}

	if(mgmt_discoverable_set(fd_mgmt, dev_id, 1, 0xFFFF) < 0){
		fprintf(stderr, "discoverable set on\n");
		return(1);
	}

	mgmt_server_start(fd_mgmt);

	fd_rfcomm = rfcomm_open(dev_id, 1);
	if(fd_rfcomm < 0){
		fprintf(stderr, "rfcomm open\n");
		return(1);
	}

	if(listen(fd_rfcomm, 1) < 0){
		fprintf(stderr, "rfcomm listen\n");
		return(1);
	}

	if(mgmt_discoverable_set(fd_mgmt, dev_id, 1, 0xFFFF) < 0){
		fprintf(stderr, "discoverable set on\n");
		return(1);
	}

	while(1){
		s = sizeof(addr_rc);
		fd_client = accept(fd_rfcomm, (struct sockaddr *)&addr_rc, &s);
		if(fd_client < 0)break;
		printf("RFCOMM ACCEPTED\n");
#ifdef COMM_TERMINAL
		terminal_loop(fd_client, "/bin/sh");
#endif
#ifdef COMM_RELAY
		relay_loop(fd_client);
#endif
		shutdown(fd_client, SHUT_RDWR);
		close(fd_client);
		printf("RFCOMM CLOSED\n");
	}
}
