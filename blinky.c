#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/hci_lib.h>
#include <bluetooth/l2cap.h>
#include <poll.h>
#include <errno.h>
#include "timer_linux.h"
#include "mtimer.h"
#include "mgmt.h"
#include "hci.h"
#include "gatt.h"
#include "gatt_server.h"

int gatt_write(void *user, const void *buf, int count);
int gatt_event(void *user, int event, uint16_t handle, uint16_t offset, void *data, int count);

int main(int argc, char *argv[]){
	int fd_mgmt, fd_hci, fd_gatt, fd_client;
	struct sockaddr_l2 addr_l2;
	gatt_server_t *gs;
	int dev_id, i, l;
	socklen_t s;
	struct pollfd fds[1];
	mtimer_t t;
	uint16_t button;
	uint8_t b = 0;

	timer_linux_init();
	mtimer_init(timer_linux_get);

	dev_id = hci_devid(argv[1]);
	if(dev_id < 0){
		if(!strncmp(argv[1], "hci", 3)){
			dev_id = atoi(argv[1] + 3);
		}
	}

	if(dev_id < 0){
		fprintf(stderr, "dev id\n");
		return(1);
	}

	fd_mgmt = mgmt_open();
	if(fd_mgmt < 0){
		fprintf(stderr, "mgmt open\n");
		return(1);
	}

	fd_hci = hci_open_dev(dev_id);
	if(fd_hci < 0){
		fprintf(stderr, "hci open\n");
		return(1);
	}

	if(ioctl(fd_hci, HCIDEVDOWN, NULL) < 0){
		fprintf(stderr, "ifdown\n");
		return(1);
	}

	mgmt_high_set(fd_mgmt, dev_id, 0);

	if(mgmt_le_set(fd_mgmt, dev_id, 1) < 0){
		fprintf(stderr, "le set on\n");
		return(1);
	}

	if(mgmt_advertising_set(fd_mgmt, dev_id, 0) < 0){
		fprintf(stderr, "advertising set off\n");
		return(1);
	}

	if(mgmt_connectable_set(fd_mgmt, dev_id, 1) < 0){
		fprintf(stderr, "connectable set on\n");
		return(1);
	}

	if(mgmt_bredr_set(fd_mgmt, dev_id, 0) < 0){
		fprintf(stderr, "br/edr set off\n");
		return(1);
	}

	if(ioctl(fd_hci, HCIDEVUP, NULL) < 0){
		fprintf(stderr, "ifup\n");
		return(1);
	}

	hci_le_send_req(fd_hci, 0x000A, "\x00", 1, 1000);

	i = hci_le_set_advertising_data(fd_hci,
		"\x02\x01\x06"
		"\x11\x07\x23\xD1\xBC\xEA\x5F\x78\x23\x15\xDE\xEF\x12\x12\x23\x15\x00\x00"
		"\x07\x09""BLINKY"
		, 29, 1000
	);
	if(i < 0){
		fprintf(stderr, "set adv data\n");
		return(1);
	}

	i = hci_le_set_scan_response_data(fd_hci,
		"\x02\x22\x06"
		, 3, 1000
	);
	if(i < 0){
		fprintf(stderr, "set scan resp data\n");
		return(1);
	}

	i = hci_le_send_req(fd_hci, 0x0006, "\xA0\x00" "\xB0\x00" "\x00" "\x00" "\x01" "\x00\x00\x00\x00\x00\x00" "\x07" "\x00", 15, 1000);
	if(i < 0){
		fprintf(stderr, "set adv parameters\n");
		return(1);
	}

	fd_gatt = gatt_open(dev_id, BDADDR_LE_PUBLIC, BT_SECURITY_LOW);
	if(fd_gatt < 0){
		fprintf(stderr, "gatt open\n");
		return(1);
	}

	if(listen(fd_gatt, 1) < 0){
		fprintf(stderr, "gatt listen\n");
		return(1);
	}

	gs = gatt_server_new(gatt_write, gatt_event, &fd_client);
	gatt_server_add_primary_service(gs, gatt_server_uuid_convert("00001523-1212-efde-1523-785feabcd123"));
	gatt_server_add_characteristic(gs, 0, gatt_server_uuid_convert("00001525-1212-efde-1523-785feabcd123"), GATT_SERVER_PROP_READ | GATT_SERVER_PROP_WRITE_CMD | GATT_SERVER_PROP_WRITE_REQ, "", 1);
	button = gatt_server_add_characteristic(gs, 0, gatt_server_uuid_convert("00001524-1212-efde-1523-785feabcd123"), GATT_SERVER_PROP_READ | GATT_SERVER_PROP_NOTIFY, "", 1);

	gatt_server_debug(gs);

	printf("READY\n");

	while(1){
		unsigned char buffer[600];
	
		i = hci_le_set_advertise_enable(fd_hci, 1, 1000);
		if(i < 0){
			fprintf(stderr, "set adv on\n");
			return(1);
		}

		printf("WAITING\n");
		s = sizeof(addr_l2);
		fd_client = accept(fd_gatt, (struct sockaddr *)&addr_l2, &s);
		printf("ACCEPTED\n");
		mtimer_timeout_clear(&t);
		while(1){
			fds[0].fd = fd_client;
			fds[0].events = POLLIN;
			if(poll(fds, 1, 50) > 0){
				l = read(fd_client, buffer, 600);
				if(l < 0)break;
				printf("READ: %d,", l);
				for(int i = 0; i < l; i++){
					printf(" %02X", (unsigned int)buffer[i]);
				}
				printf("\n");
				gatt_server_feed(gs, buffer, l);
			}
			if(mtimer_timeout(&t)){
				if(WEXITSTATUS(system("/bin/button >/dev/null 2>&1")) == 1){
					if(b == 0){
						printf("BUTTON PRESSED\n");
						b = 1;
						gatt_server_value_set(gs, button, &b, 1);
					}
				}else{
					if(b == 1){
						printf("BUTTON RELEASED\n");
						b = 0;
						gatt_server_value_set(gs, button, &b, 1);
					}
				}
				mtimer_timeout_add(&t, 100);
			}
		}
		shutdown(fd_client, SHUT_RDWR);
		close(fd_client);
	}
}

int gatt_write(void *user, const void *buf, int count){
	int client_fd;
	client_fd = *(int *)user;
	printf("WRITE: %d,", count);
	for(int i = 0; i < count; i++){
		printf(" %02X", (unsigned int)((uint8_t *)buf)[i]);
	}
	printf("\n");
	return(write(client_fd, buf, count));
}

int gatt_event(void *user, int event, uint16_t handle, uint16_t offset, void *data, int count){
/*	printf("GATT EVENT: %d, HANDLE: %04x, OFFSET: %u, DATA:", event, (unsigned int)handle, (unsigned int)offset);
	for(int i = 0; i < count; i++){
		printf(" %02X", (unsigned int)((uint8_t *)data)[i]);
	}
	printf("\n");*/
	if(handle == 0x0003){
		if(((uint8_t *)data)[0]){
			system("echo 255 >/sys/class/leds/yellow:debug/brightness 2>/dev/null");
			printf("LED ON\n");
		}else{
			system("echo 0 >/sys/class/leds/yellow:debug/brightness 2>/dev/null");
			printf("LED OFF\n");
		}
	}
	return(0);
}

